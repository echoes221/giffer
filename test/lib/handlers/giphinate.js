'use strict'

/* eslint-env node, mocha */

const sinon = require('sinon')
const supertest = require('supertest')
const nock = require('nock')
const express = require('express')
const giphinateHandler = require('../../../lib/handlers/giphinate')

describe('giphinateHandler', () => {
  beforeEach(() => {
    const app = express()
    const findStub = sinon.stub()
    findStub.withArgs({where: {query: 'foo'}}).returns(new Promise((resolve, reject) => {
      resolve({
        url: 'http://foobar.com',
        query: 'foo',
      })
    }))
    findStub.withArgs({where: {query: 'baz'}}).returns(new Promise((resolve, reject) => {
      resolve(null)
    }))

    const createStub = sinon.stub().returns(new Promise((resolve, reject) => {
      resolve({ dataValues: {
        url: 'http://foobaz.com',
        query: 'baz',
      }})
    }))

    const removeStub = sinon.stub().returns(new Promise((resolve, reject) => {
      resolve(0)
    }))

    this.dbInstance = {
      findOne: findStub,
      create: createStub,
      destroy: removeStub,
    }
    const handler = giphinateHandler(this.dbInstance)

    app.get('/:queryText', handler.getRoute)
    app.delete('/:queryText', handler.deleteRoute)

    nock('http://api.giphy.com')
      .get('/v1/gifs/search')
      .query({
        q: 'baz',
        'api_key': 'dc6zaTOxFJmzC',
        limit: 1,
      })
      .reply(200, '{"data":[{"embed_url":"http://foobaz.com"}]}')

    this.agent = supertest.agent(app)
  })

  describe('router.get', () => {
    it('should return a giphyURL from a query', (done) => {
      this.agent.get('/foo')
        .send()
        .expect(200, {
          url: 'http://foobar.com',
          query: 'foo',
        })
        .end(done)
    })

    it('should return a giphy url from query if not in db by fetching', (done) => {
      this.agent.get('/baz')
        .send()
        .expect({
          url: 'http://foobaz.com',
          query: 'baz',
        })
        .end(done)
    })
  })

  describe('router.delete', () => {
    it('should remove a value based off the query', (done) => {
      this.agent.delete('/foo')
        .send()
        .expect(200, {
          recordsRemoved: 0,
        })
        .end(done)
    })
  })
})
