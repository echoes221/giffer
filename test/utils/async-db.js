'use strict'

/* eslint-env node, mocha */

const expect = require('chai').expect
const sinon = require('sinon')
const AsyncDB = require('../../lib/utils/async-db')

describe('AsyncDB', () => {
  beforeEach(() => {
    const findStub = sinon.stub()
    findStub.withArgs({where: {foo: 'foo'}}).returns(new Promise((resolve, reject) => {
      resolve({
        url: 'http://foobar.com',
        query: 'foo',
      })
    }))
    findStub.withArgs({where: {baz: 'baz'}}).returns(new Promise((resolve, reject) => {
      resolve(null)
    }))

    const createStub = sinon.stub().returns(new Promise((resolve, reject) => {
      resolve({ dataValues: {
        url: 'http://foobar.com',
        query: 'foo',
      }})
    }))

    const removeStub = sinon.stub().returns(new Promise((resolve, reject) => {
      resolve(0)
    }))

    this.fetcherStub = sinon.stub().returns(new Promise((resolve, reject) => {
      resolve({
        url: 'http://foobar.com',
        query: 'foo',
      })
    }))

    this.dbInstance = {
      findOne: findStub,
      create: createStub,
      destroy: removeStub,
    }

    this.db = new AsyncDB(this.dbInstance, this.fetcherStub)
  })

  describe('find', () => {
    describe('When the value is in the DB', () => {
      it('should return the value from the db and not fetch', (done) => {
        this.db.find({ foo: 'foo' }).then((data) => {
          expect(this.fetcherStub.called).to.equal(false)
          expect(this.dbInstance.create.called).to.equal(false)
          expect(data).to.deep.equal({
            url: 'http://foobar.com',
            query: 'foo',
          })

          done()
        })
      })
    })

    describe('When the value is missing from the DB', () => {
      it('should fetch the value from the fetcher and store in db', (done) => {
        this.db.find({ baz: 'baz' }).then((data) => {
          expect(this.fetcherStub.called).to.equal(true)
          expect(this.dbInstance.create.called).to.equal(true)
          expect(data).to.deep.equal({
            url: 'http://foobar.com',
            query: 'foo',
          })

          done()
        })
      })
    })
  })

  describe('remove', () => {
    it('should call the remove method of the DB', (done) => {
      this.db.remove({ foo: 'foo' }).then((data) => {
        expect(this.dbInstance.destroy.called).to.equal(true)

        done()
      })
    })
  })
})
