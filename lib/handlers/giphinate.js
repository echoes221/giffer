'use strict'

// eslint-disable-next-line no-unused-vars
const request = require('request')
const api = require('../../config').api.giphy
const AsyncDB = require('../utils/async-db')
const _ = require('lodash')

const giphinateHandler = (giphyDB) => {
  const requiredFields = ['url', 'query']
  const db = new AsyncDB(giphyDB, (data) => {
    const query = data.query
    const parsedQuery = query.split(' ').map(string => encodeURIComponent(string)).join('+')
    const endpoint = `${api.endpoint}?q=${parsedQuery}&api_key=${api.key}&limit=1`

    return new Promise((resolve, reject) => {
      request(endpoint, (err, res, body) => {
        if (err) {
          return reject(err)
        }

        try {
          body = JSON.parse(body).data[0]

          resolve({
            query,
            url: body.embed_url,
          })
        } catch (e) {
          reject({ err: e, reason: 'No Gif Found!' })
        }
      })
    })
  })

  /**
   * GET giphy URL based on queryText
   *
   * @method get
   *
   * @param {Object} req
   * @param {String} req.params.queryText
   * @param {Object} res
   */
  const getRoute = (req, res) => {
    const query = decodeURIComponent(req.params.queryText)

    db.find({ query }).then((obj) => {
      res.json(_.pick(obj, requiredFields))
    }).catch((err) => {
      res.status(404).send(err.reason || 'No Data Found!')
    })
  }

  /**
   * DELETE giphy URL based on queryText
   *
   * @method delete
   *
   * @param {Object} req
   * @param {String} req.params.queryText
   * @param {Object} res
   */
  const deleteRoute = (req, res) => {
    const query = decodeURIComponent(req.params.queryText)

    db.remove({ query }).then((data) => {
      res.json({
        recordsRemoved: data,
      })
    }).catch((err) => {
      res.send(500).status(err.reason || 'Something Went Wrong!')
    })
  }

  return {
    getRoute,
    deleteRoute,
  }
}

module.exports = giphinateHandler
