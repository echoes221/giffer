'use strict'

/**
 * Similar to async-cache instead of checking the cache and fetching from a source
 * It checks the db and calls from the fetcher if the record in db is missing
 *
 * @class AsyncDB
 * @param {Object} db sequelize instance
 * @param {Function} fetcher promise based fetcher
 */
class AsyncDB {
  constructor (db, fetcher) {
    this.db = db
    this.fetcher = fetcher
  }

  /**
   * Tries to find a record in the db, if not calls fetcher to find data, stores
   * record in db for future use
   *
   * @method find
   * @for AsyncDB
   *
   * @param {Object} query
   * @return {Promise}
   */
  find (query) {
    return new Promise((resolve, reject) => {
      this.db.findOne({ where: query }).then((data) => {
        if (!data) {
          this.fetcher(query).then((result) => {
            this.db.create(result).then((entry) => {
              resolve(entry.dataValues)
            })
          }).catch((err) => {
            reject(err)
          })
        } else {
          resolve(data)
        }
      }).catch((err) => {
        reject(err)
      })
    })
  }

  /**
   * Will remove a record from the db
   * returns count of rows deleted on resolution
   *
   * @method remove
   * @for AsyncDB
   *
   * @param {Object} query
   * @return {Promise}
   */
  remove (query) {
    return new Promise((resolve, reject) => {
      this.db.destroy({ where: query }).then((data) => {
        resolve(data)
      }).catch((err) => {
        reject(err)
      })
    })
  }
}

module.exports = AsyncDB
