'use strict'

/**
 * Server config
 * TODO implement config module for env configs
 */
const config = {
  // Server port
  port: 8000,

  // API
  api: {
    giphy: {
      endpoint: 'http://api.giphy.com/v1/gifs/search',
      key: 'dc6zaTOxFJmzC',
    },
  },
}

module.exports = config
