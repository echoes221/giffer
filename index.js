'use strict'

const config = require('./config')
const express = require('express')
const bodyParser = require('body-parser')
const schemas = require('./lib/schemas')()
const menna = require('menna')
const giphinateHandler = require('./lib/handlers/giphinate')(schemas.Giphys)
const app = express()
const port = process.env.PORT || config.port

// Catch any unhandled errors
process.on('unhandledRejection', (reason, promise) => {
  menna.error(`Unhandled rejection at: ${promise}, reason: ${reason}`)
})

// Register Middlewares
app.use(bodyParser.json())
app.use((req, res, next) => {
  const reqTime = Date.now()
  const info = `Method: ${req.method}, Path: ${req.path}`

  menna.info(`Requested: ${info}, TimeStamp: ${reqTime}`)

  const done = () => {
    const now = Date.now()
    return menna.info(`Response: ${info}, Status: ${res.statusCode}, Timestamp: ${now}, Took: ${now - reqTime}`)
  }

  res.on('finish', done)
  res.on('close', done)

  next()
})

// Register Routes
app.get('/:queryText', giphinateHandler.getRoute)
app.delete('/:queryText', giphinateHandler.deleteRoute)

// Listen
app.listen(port, function () {
  console.log(`Listening on port ${port}`)
})
